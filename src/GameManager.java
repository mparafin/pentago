import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GameManager {
	
	//depth of minimax GameTree
	private static int depth1 = -1, depth2 = -1;
	private static boolean alpha1 = true, alpha2 = true;

	private static Board board;
	private static int currentPlayer = 0;

	private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));


	private static void PvAI()
	{
        String input = null;

        //Create opponent
		AI ai = new AI(depth1);

		GameTreeNode currentRoot = new GameTreeNode(board);

		int winner = 0;
		//while no current winner
		while(winner == 0)
		{
			//if it's the user's turn
			if(currentPlayer == 1)
			{
				System.out.println("Enter <row> <col> <rotation>:");
				 try {
					 input = in.readLine();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }

				 if(input != null)
				 {
				 	winner = parseInput(input);
					 if (winner == -1){
					 	winner = 0;
					 	currentPlayer = 1;
					 	continue;
					 }
					 currentRoot = new GameTreeNode(board);
					 board.printBoard();
				 }
			}
			else	//if AI's turn
			{
				System.out.println("Computing AI move...");

				//timestamp, used to calculate run time
				long startTime = System.currentTimeMillis();
				//computes move with best minimax value given the depth, returns the int of the final maximum value at the root of the game tree
				GameTreeNode AIMove = ai.minimaxMove(currentRoot, false, alpha1, 2);
				long endTime = System.currentTimeMillis();

				System.out.println("AI chooses the following move: " + AIMove.getBoard().getOperator() +
						"\nwhich took " + (endTime-startTime) + "ms to compute.");
				winner = parseInput(AIMove.getBoard().getOperator());
				currentRoot = AIMove;
				board.printBoard();
			}

		}
		System.out.println("Player " + winner + " won!");
	}

	private static void AIvAI() {
		//create players
		AI ai1 = new AI(depth1);
		AI ai2 = new AI(depth2);

		GameTreeNode currentRoot = new GameTreeNode(board);
		currentPlayer = 1;

		int winner = 0;
		while (winner == 0) {

			System.out.println("Computing move of player " + currentPlayer + "...");

			boolean alpha = currentPlayer==1? alpha1 : alpha2;
			AI currentAI = currentPlayer==1? ai1 : ai2;
			//timestamp, used to calculate run time
			long startTime = System.currentTimeMillis();
			GameTreeNode AIMove = currentAI.minimaxMove(currentRoot, currentPlayer==1, alpha, currentPlayer);
			long endTime = System.currentTimeMillis();

			System.out.println("AI chooses the following move: " + AIMove.getBoard().getOperator() +
					"\nwhich took " + (endTime-startTime) + "ms to compute.");
			winner = parseInput(AIMove.getBoard().getOperator());
			currentRoot = AIMove;
			board.printBoard();
		}
		System.out.println("Player " + winner + " won!");
	}

	private static int parseInput(String input)
	{
		int winner = 0;
		String[] move = input.split(" ");
		try{
			if(board.isValidMove(Integer.parseInt(move[0]), Integer.parseInt(move[1]), move[2]))
			{
				board.placePawn(currentPlayer, Integer.parseInt(move[0]), Integer.parseInt(move[1]));
				//change player
				currentPlayer = currentPlayer % 2 + 1;
				//check if winner before rotating
				winner = board.determineWinner();
				if (winner == 0) {
					board.rotate(move[2].charAt(0));
					winner = board.determineWinner();
				} else {
					return winner;
				}
			}
			else
			{
				System.out.println("Invalid move attempted.");
				return -1;
			}

		}catch(Exception e)
		{
			System.out.println("Could not interpret input(.");
		}
		return winner;
	}

	private static void determineStarting()
	{
		System.out.println("Are you or the AI going first? (1 = you, 2 = AI)");
        String input = null;
		//get starting player
        while(currentPlayer == 0 || currentPlayer > 2)
        {
        	try {
    			input = in.readLine();
    			currentPlayer = Integer.parseInt(input);
    		}
    		catch(Exception e)
    		{
    			System.out.println("You must enter an integer (1 or 2).");
    		}
        }
	}

	private static boolean askIfPvAI() {
		String input = null;

		try {
			do {
				System.out.println("Please determine mode: Player vs AI (1) or AI vs AI (2)?");
				try {
					input = in.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} while (!input.equals("1") && !input.equals("2"));

			return input.equals("1");
		} catch (NullPointerException e) {
			return false;
		}
	}

	private static void readDepth(boolean AIvAI) {
		if (AIvAI) System.out.println("For the 1st player:");
		System.out.println("What is going to be the depth of the AI's sight into the game tree?");
		String input = null;

		while(depth1 <= 0)
		{
			System.out.println("Please enter a number: ");
			try {
				input = in.readLine();
				depth1 = Integer.parseInt(input);
			}
			catch(Exception e)
			{
				System.out.println("You must enter an integer.");
			}
		}
		if (AIvAI) {
			System.out.println("For the 2nd player:");
			System.out.println("What is going to be the depth of the AI's sight into the game tree?");

			while(depth2 <= 0)
			{
				System.out.println("Please enter a number: ");
				try {
					input = in.readLine();
					depth2 = Integer.parseInt(input);
				}
				catch(Exception e)
				{
					System.out.println("You must enter an integer.");
				}
			}

		}
	}


	private static void readAlpha(boolean AIvAI) {
		if (AIvAI) System.out.println("For the 1st player:");
		System.out.println("Do you want the AI to use alpha-beta pruning?");
		String input = null;

		System.out.println("It will, by default. Enter anything if you want it to use the simple minimax: ");
		try {
			input = in.readLine();
			if (input.length() >0) alpha1 = false;
		} catch (Exception e) {
			alpha1 = true;
		}

		if (AIvAI) {
			System.out.println("For the 2nd player:");
			System.out.println("Do you want the AI to use alpha-beta pruning?");

			System.out.println("It will, by default. Enter anything if you want it to use the simple minimax: ");
			input = null;
			try {
				input = in.readLine();
				if (input.length() > 0) alpha2 = false;
			} catch (Exception e) {
				alpha2 = true;
			}
		}
	}

	public static void main(String[] args)
	{
		board = new Board();

/*
		board.placePawn(1, 1, 1);
//		board.placePawn(2, 2, 1);
//		board.placePawn(2, 3, 1);
//		board.placePawn(2, 4, 1);
//		board.placePawn(2, 5, 1);
		board.placePawn(1, 1, 2);
		board.placePawn(1, 2, 2);
//		board.placePawn(2, 3, 2);
		board.placePawn(1, 3, 3);
		board.placePawn(1, 4, 4);
		board.placePawn(1, 6, 6);
*/

		System.out.println("Value test for: ");
		board.printBoard();
		System.out.println("Value is " + board.evaluate());
		System.out.println("Determinewinner(): " + board.determineWinner());

		boolean pvAI = askIfPvAI();
		readDepth(!pvAI);
		readAlpha(!pvAI);
		if (pvAI) {
			determineStarting();
			System.out.println("PvAI game started!\n");
			board.printBoard();
			PvAI();
		} else {
			System.out.println("AIvsAI game started!\n");
			board.printBoard();
			AIvAI();
		}

	}
}
