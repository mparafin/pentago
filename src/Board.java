class Board {
	private int[][] slots;
	private String operator = "";	//move that defines this board compared to the parent of this board in the game tree
	private int value = 0;
	
	Board()
	{
		slots = new int[6][6];
		blankBoard();
	}

	//used for copying game board moves
	Board(int[][] moves)
	{
		slots = new int[6][6];
		for(int i = 0; i < 6; i++)
			System.arraycopy(moves[i], 0, slots[i], 0, 6);
	}

	void setOperator(String move)
	{
		operator = move;
	}

	String getOperator()
	{
		return operator;
	}

	int[][] getSlots() {
		return slots;
	}

	public int getValue() {
		return value;
	}

	private void blankBoard()
	{
		for(int i = 0; i < slots.length; i++)
		{
			for(int j = 0; j < slots[i].length; j++)
				slots[i][j] = 0;
		}
	}

	void placePawn(int player, int row, int col)
	{
		//use -1 since input is from 1-6, not 0-5
		slots[row - 1][col - 1] = player;
	}

	boolean isValidMove(int row, int col, String rot)
	{
		return (slots[row - 1][col - 1] == 0) && rot.matches("[ABCDabcd]");
	}

	//rotate matrix clockwise
	private static int[][] rotateQuadrant(int[][] mat) {
		final int m = mat.length;
		final int n = mat[0].length;
		int[][] ret = new int[n][m];
		for (int r = 0; r < m; r++) {
			for (int c = 0; c < n; c++) {
				ret[c][m-1-r] = mat[r][c];
			}
		}
		return ret;
	}

	private static int[][] rotateQuadrant(int[][] mat, boolean counterclockwise) {
		if (counterclockwise) return rotateQuadrant(rotateQuadrant(rotateQuadrant(mat)));
		return rotateQuadrant(mat);
	}

	//used for minimax algorithm to determine the value of a gameboard
	int evaluate()
	{
		int value = 0;
		int wstreak=0, bstreak=0;

		//count horizontal doubles
		for(int i = 0; i < 6; i++)	//go down row
		{
			for(int j = 0; j < 5; j++)	//count doubles
			{
				if(slots[i][j] == 1 && slots[i][j+1] == 1)
				{
					wstreak++;
					if (wstreak >5) wstreak = 5;
					value += wstreak*wstreak*wstreak;
				}
				else
					wstreak = 0;

				if(slots[i][j] == 2 && slots[i][j+1] == 2)
				{
					bstreak++;
					if (bstreak >5) bstreak = 5;
					value -= bstreak*bstreak*bstreak;
				}
				else
					bstreak = 0;
			}
		}
		wstreak = 0; bstreak = 0;

		//count vertical doubles
		for(int i = 0; i < 6; i++)	//go down columns
		{
			for(int j = 0; j < 5; j++)	//count doubles
			{
				if(slots[j][i] == 1 && slots[j+1][i] == 1)
				{
					wstreak++;
					if (wstreak >5) wstreak = 5;
					value += wstreak*wstreak*wstreak;
				}
				else
					wstreak = 0;

				if(slots[j][i] == 2 && slots[j+1][i] == 2)
				{
					bstreak++;
					if (bstreak >5) bstreak = 5;
					value -= bstreak*bstreak*bstreak;
				}
				else
					bstreak = 0;
			}
		}
		wstreak = 0; bstreak = 0;

		//count main diagonal up-left to down-right
		for(int i = 0; i < 5; i++)
		{
			if(slots[i][i] == 1 && slots[i+1][i+1] == 1)
			{
				wstreak++;
				if (wstreak >5) wstreak = 5;
				value += wstreak*wstreak*wstreak;
			}
			else
				wstreak = 0;

			if(slots[i][i] == 2 && slots[i+1][i+1] == 2)
			{
				bstreak++;
				if (bstreak >5) bstreak = 5;
				value -= bstreak*bstreak*bstreak;
			}
			else
				bstreak = 0;
		}
		wstreak = 0; bstreak = 0;

		//count main diagonal up-right to down-left
		for(int i = 0; i < 5; i++)
		{
			if(slots[i][5-i] == 1 && slots[i+1][4-i] == 1)
			{
				wstreak++;
				if (wstreak >5) wstreak = 5;
				value += wstreak*wstreak*wstreak;
			}
			else
				wstreak = 0;

			if(slots[i][5-i] == 2 && slots[i+1][4-i] == 2)
			{
				bstreak++;
				if (bstreak >5) bstreak = 5;
				value -= bstreak*bstreak*bstreak;
			}
			else
				bstreak = 0;
		}
		this.value = value;
		return value;
	}

	void printBoard()
	{
		System.out.println("Current board:\n");
		for(int i = 0; i < 6; i++)
		{
			String lineString = "";
			for(int j = 0; j < 6; j++)
			{
				if(slots[i][j] == 1)
					lineString += "W ";
				else if(slots[i][j] == 2)
					lineString += "B ";
				else
					lineString += "O ";
				if(j == 2)
					lineString += " ";
			}
			System.out.println(lineString);
			if(i == 2)
				System.out.println();
		}
	}


	void rotate(char key) {
		int[][] quadrant = new int[3][3];
		int ioff = 0, joff = 0;		// offsets used to determine which quadrant we're rotating
		boolean cclock = false;		// are we rotating counter-clockwise?

		if (Character.isUpperCase(key)) cclock = true;
		key = Character.toLowerCase(key);
		switch (key) {
			case 'a':
				break;
			case 'b':
				joff = 3;
				break;
			case 'c':
				ioff = 3;
				break;
			case 'd':
				ioff = 3;
				joff = 3;
				break;
			default:
				return;
		}

		//set quadrant to correct values
		for(int i = 0; i < 3; i++)
		{
			System.arraycopy(slots[i + ioff], joff, quadrant[i], 0, 3);
		}
		//rotate quadrant
		quadrant = rotateQuadrant(quadrant, cclock);
		//put quadrant back in gameboard
		for(int i = 0; i < 3; i++)
		{
			System.arraycopy(quadrant[i], 0, slots[i + ioff], joff, 3);
		}
	}


	/**
	 * @return int - 0: no winner, 1: Player 1 won, 2: Player 2 won
	 */
	int determineWinner()
	{
		int temp;
		int counter = 0;
		// 5s in rows
		for(int i = 0; i < 6; i++)
		{
			if (slots[i][0] == 0 && slots[i][1] == 0) continue;
			temp = slots[i][0];

			for (int j = 1; j < 6; j++) {
				if (temp != 0 && slots[i][j] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[i][j];
				if (6-j+counter < 5) break;
			}
		}

		// 5s in columns
		for(int j = 0; j < 6; j++)
		{
			if (slots[0][j] == 0 && slots[1][j] == 0) continue;
			temp = slots[0][j];

			for (int i = 1; i < 6; i++) {
				if (temp != 0 && slots[i][j] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[i][j];
				if (counter-j+1 < 0) break;
			}
		}

/*
		0 W 0  0 0 0
		0 0 W  0 0 0
		0 0 0  W 0 0

		0 0 0  0 W 0
		0 0 0  0 0 W
		0 0 0  0 0 0
*/
		temp = slots[0][1];
		if (slots[0][1] != 0) {
			for (int i = 1; i < 5; i++) {
				if (temp != 0 && slots[i][i+1] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[i][i+1];
				if (counter-i < 0) break;
			}
		}
/*
		0 0 0  0 0 0
		W 0 0  0 0 0
		0 W 0  0 0 0

		0 0 W  0 0 0
		0 0 0  W 0 0
		0 0 0  0 W 0
*/
		temp = slots[1][0];
		if (slots[1][0] != 0) {
			for (int i = 1; i < 5; i++) {
				if (temp != 0 && slots[i+1][i] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[i+1][i];
				if (counter-i < 0) break;
			}
		}
		
/*
		0 0 0  0 W 0
		0 0 0  W 0 0
		0 0 W  0 0 0

		0 W 0  0 0 0
		W 0 0  0 0 0
		0 0 0  0 0 0
*/
		temp = slots[0][4];
		if (slots[0][4] != 0) {
			for (int i = 1; i < 5; i++) {
				if (temp != 0 && slots[i][4-i] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[i][4-i];
				if (counter-i < 0) break;
			}
		}
		
/*
		0 0 0  0 0 0
		0 0 0  0 0 W
		0 0 0  0 W 0

		0 0 0  W 0 0
		0 0 W  0 0 0
		0 W 0  0 0 0
*/
		temp = slots[1][5];
		if (slots[1][5] != 0) {
			for (int i = 1; i < 5; i++) {
				if (temp != 0 && slots[i+1][5-i] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[i+1][5-i];
				if (counter-i < 0) break;
			}
		}

/*
		W 0 0  0 0 0
		0 W 0  0 0 0
		0 0 W  0 0 0

		0 0 0  w 0 0
		0 0 0  0 W 0
		0 0 0  0 0 0
*/
		temp = slots[0][0];
		if (slots[0][0] != 0 && slots[1][1] != 0) {
			for (int i = 1; i < 5; i++) {
				if (temp != 0 && slots[i][i] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[i][i];
				if (counter-i+1 < 0) break;
			}
		}
		
/*
		0 0 0  0 0 W
		0 0 0  0 W 0
		0 0 0  W 0 0

		0 0 w  0 0 0
		0 w 0  0 0 0
		0 0 0  0 0 0
*/
		temp = slots[5][5];
		if (slots[5][5] != 0 && slots[4][4] != 0) {
			for (int i = 1; i < 5; i++) {
				if (temp != 0 && slots[5-i][5-i] == temp) counter++;
				else counter = 0;
				if (counter >= 4) return temp;
				temp = slots[5-i][5-i];
				if (counter-i+1 < 0) break;
			}
		}

		//no winner found
		return 0;
	}
}
