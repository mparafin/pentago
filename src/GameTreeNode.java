import java.util.ArrayList;
import java.util.List;

/* A class that serves as an overlay to the Board class to separate functionality connected with the game tree search*/
class GameTreeNode {
	private Board board;

	GameTreeNode(Board board)
	{
		this.board = board;
	}

	List<GameTreeNode> getChildren(int player)
	{
		List<GameTreeNode> children = new ArrayList<>();
		for (int i = 1; i <= 6; i++) {
			for (int j = 1; j <= 6; j++) {
				//if valid move, add all boards with this move and all rotations to the list
				if (board.isValidMove(i, j, "A")) {
				 	children.add(createNode(i, j, 'A', player));
					children.add(createNode(i, j, 'B', player));
					children.add(createNode(i, j, 'C', player));
					children.add(createNode(i, j, 'D', player));
					children.add(createNode(i, j, 'a', player));
					children.add(createNode(i, j, 'b', player));
					children.add(createNode(i, j, 'c', player));
					children.add(createNode(i, j, 'd', player));
				}
			}
		}
		return children;
	}

	private GameTreeNode createNode(int i, int j, char dir, int player) {
		Board tempBoard = new Board(board.getSlots());
		tempBoard.placePawn(player, i, j);
		tempBoard.rotate(dir);
		tempBoard.setOperator(i + " " + j + " " + dir);
		return new GameTreeNode(tempBoard);
	}

	Board getBoard()
	{
		return board;
	}
}
