import java.util.List;


class AI {
	private int depth, bestMoveId;

	AI(int d)
	{
		depth = d;
	}

	GameTreeNode minimaxMove(GameTreeNode currentNode, boolean maximizingPlayer, boolean alphaBeta, int player) {
		List<GameTreeNode> moves = currentNode.getChildren(player);
		int bestValue = maximizingPlayer? Integer.MIN_VALUE : Integer.MAX_VALUE;

		for (int i = 0, movesSize = moves.size(); i < movesSize; i++) {
			int value;
			if (alphaBeta) {
				value = minimax_ab(depth - 1, moves.get(i), !maximizingPlayer, player%2+1, Integer.MIN_VALUE, Integer.MAX_VALUE);		//alpha-beta
			} else {
				value = minimax(depth - 1, moves.get(i), !maximizingPlayer, player%2+1);												// normal
			}
			if (maximizingPlayer && value > bestValue) {
				bestValue = value;
				bestMoveId = i;
			} else if (!maximizingPlayer && value < bestValue) {
				bestValue = value;
				bestMoveId = i;
			}
			System.out.println("Move " + i + ", value: " + value + ", bestValue: " + bestValue);
		}
		System.out.println("Returning move " + bestMoveId + ", value: " + moves.get(bestMoveId).getBoard().getValue() + ", bestValue: " + bestValue);
		return moves.get(bestMoveId);
	}

	private int minimax(int depth, GameTreeNode currentNode, boolean maximizingPlayer, int player) {

		//leaf node of tree
		int winner = currentNode.getBoard().determineWinner();
		if (winner == 1) return 1000000;
		if (winner == 2) return -1000000;
		if(depth == 0)
		{
			return currentNode.getBoard().evaluate();
		}

		int bestValue = maximizingPlayer? Integer.MIN_VALUE : Integer.MAX_VALUE;

		List<GameTreeNode> childNodes = currentNode.getChildren(player);

		if (maximizingPlayer) {
			for (GameTreeNode childNode : childNodes) {
				int currentValue = minimax(depth - 1, childNode, false, player%2+1);
				if (currentValue > bestValue) bestValue = currentValue;
			}
		} else {
			for (GameTreeNode childNode : childNodes) {
				int currentValue = minimax(depth -1, childNode, true, player%2+1);
				if (currentValue < bestValue) bestValue = currentValue;
			}
		}
		return bestValue;
	}

	private int minimax_ab(int depth, GameTreeNode currentNode, boolean maximizingPlayer, int player, int alpha, int beta) {

		//leaf node of tree
		int winner = currentNode.getBoard().determineWinner();
		if (winner == 1) return 1000000;
		if (winner == 2) return -1000000;
		if(depth == 0)
		{
			return currentNode.getBoard().evaluate();
		}

		List<GameTreeNode> childNodes = currentNode.getChildren(player);

		if (maximizingPlayer) {
			for (GameTreeNode childNode : childNodes) {
				int currentValue = minimax_ab(depth - 1, childNode, false, player%2+1, alpha, Integer.MAX_VALUE);
				if (currentValue > alpha) alpha = currentValue;
				if (alpha >= beta) return alpha;
			}
			return alpha;
		} else {
			for (GameTreeNode childNode : childNodes) {
				int currentValue = minimax_ab(depth -1, childNode, true, player%2+1, Integer.MIN_VALUE, beta);
				if (currentValue < beta) beta = currentValue;
				if (alpha >= beta) return beta;
			}
			return beta;
		}
	}
}
